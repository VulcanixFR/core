# Core

## Setting up the dev environment

To install the necessary packages, run `npm i` 

This repository has a `.vscode` folder with a build task and a run task
to debug the app with VsCode.

An HTML documentation can be generated with the help of `typedoc`.
To generate the docs, run `npm run mkdoc`.

Each declaration in the code has to be commented with the standard tsdoc 
comment bloc. You can see below an example for a function:

```ts
/**
 * Splits a string at each comma
 * 
 * @param toSplit String to split  
 * @return The elements separated by the commas
*/
function splitComma (toSplit: string): string[] { return arg1.split(","); }
```

The camel case has to be used. Types have their first letter in uppercase. 
Private attributes in classes have to begin by an underscore for debug ease.
Protected attributes doesn't have to but it's prefered.

```ts
// Wrong
let long_var_name: string = "...";
type phoneNumber = string;
class A {
    private selfDestruct () {  }
}

// OK
let longVarName: string = "...";
type PhoneNumber = string;
class A {
    private _selfDestruct () {  }
}
```

**NB**: types can be lowercased if they are physical units (ex: ms, m, ...)

Don't hesitate to leave some space in your code to improve readability,
and comment part of your algorithms (with `//` comments) for you to 
remeber later what you did.

## The idea behind core 

This diagram (in French) describes the main idea of Core's job.

![diagram](./public/img/core_idea.png)

If you wonder "Why use Node.Js and not Python ?", 
well the "Autovision" project written in Node.JS has proven to be lightweight and
reliable while managing subprocesses and TCP/IP sockets at the same time.

This app will not need threading, the event loop is fast enough to do it's duty, 
and the ease of writing async code and event-based code 
natively will be a great help.

