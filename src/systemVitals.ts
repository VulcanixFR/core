import { osInfo, currentLoad, mem, fsSize, usb, networkInterfaces, Systeminformation, time } from "systeminformation";
import { CockpitClient } from "./cockpit/client";

/** Miliseconds */ 
type ms = number;

/** The Vitals gathered */
export type Vitals = {
    os: { hostname: string, distro: string, release: string, kernel: string },
    /** CPU load percentage */
    load: number,
    ram: { 
        /** Number of byte of the system ram */
        total: number, 
        /** Number of bytes used */
        used: number, 
        /** Percentage of ram use */
        use: number     
    },
    fs: { 
        /** Mount point of the volume */
        mount: string,
        /** Format of the volume */
        type: string,
        /** Percentage of disk use */        
        use: number,
        /** Number of bytes used */
        used: number,
        /** Total number of bytes of the volume */
        size: number
    }[],
    usb: {
        bus: number,
        deviceId: number,
        name: string
    }[],
    net: {
        /** Name of the interface */
        iface: string,
        /** IPv4 */
        ip4: string,
        /** IPv6 */
        ip6: string,
    }[]
};


/**
 * Wrapper for the systeminformation package
 */
export class SystemVitals {

    public timeout: ms = 500;
    public vitals: Vitals;
    
    private _looping: boolean = false;
    private _listeners: CockpitClient[] = [];

    constructor () {

        this.vitals = {
            os: { distro: "N/A", hostname: "N/A", kernel: "N/A", release: "N/A" },
            fs: [],
            net: [],
            usb: [],
            load: 0,
            ram: { total: 0, use: 0, used: 0 }
        };

    }

    /**
     * Attaches a listener to the vitals update 
     * @param client 
     */
    subscribe (client: CockpitClient) {
        if (this._listeners.indexOf(client) != -1) return; // Not adding twice the same callback

        this._listeners.push(client);

        let payload = JSON.stringify(this.vitals);
        client.sendPayload("vitals_auto", payload);
        
        if (!this._looping) {
            this._looping = true;
            this._loop();
            this._loopSlow();
        }
    }

    /**
     * Detaches a listener from the vitals update 
     * @param client The client
     */
    unsubscribe (client: CockpitClient) {
        this._listeners = this._listeners.filter(l => l != client);
        this._looping = this._listeners.length > 0;
    }

    /**
     * Updates and returns the vitals
     * @returns The latest vitals
     */
    async latestVitals (): Promise<Vitals> {
        await this.updateSlow();
        await this.update();
        return this.vitals;
    }

    /**
     * Update the SystemVitals internal data
     */
    async update () {

        let { distro, hostname, kernel, release } = await osInfo();
        
        let { total, used } = await mem();
        let use = 100 * used / total;

        let load = (await currentLoad()).currentLoad;

        this.vitals.os = { distro, hostname, kernel, release };
        this.vitals.ram = { total, use, used };
        this.vitals.load = load;

    }
    /**
     * Update the SystemVitals internal data
     * 
     * Only for the "fs", "usb" and "net" attributes which are slower to obtain
     */
    async updateSlow () {

        let _fs = await fsSize();
        let _usb = await usb();
        let _net = <Systeminformation.NetworkInterfacesData[]>(await networkInterfaces());

        this.vitals.fs = _fs.map(({ type, mount, use, used, size }) => ({ type, mount, use, used, size }));
        this.vitals.usb = _usb.map(({ bus, deviceId, name }) => ({ bus, deviceId, name }));
        this.vitals.net = _net.map(({iface, ip4, ip6}) => ({ iface, ip4, ip6 }));

    }

    /**
     * Keeps refreshing the internal data while there are listeners
     */
    private async _loop () {

        if (!this._looping) return;

        await this.update();
        let payload = JSON.stringify(this.vitals);
        let clientsToRemove: CockpitClient[] = [];

        this._listeners.forEach(sub => {
            let success = sub.sendPayload("vitals_auto", payload);
            if (!success) clientsToRemove.push(sub);
        });

        this._listeners = this._listeners.filter(
            sub => clientsToRemove.indexOf(sub) == -1
        );

        await sleep(this.timeout);

        this._loop();

    }

    /**
     * Keeps refreshing the internal data while there are listeners
     * 
     * Only for the "fs", "usb" and "net" attributes which are slower to obtain
     */
    private async _loopSlow () {

        if (!this._looping) return;

        await this.updateSlow();
        let payload = JSON.stringify(this.vitals);
        let clientsToRemove: CockpitClient[] = [];

        this._listeners.forEach(sub => {
            let success = sub.sendPayload("vitals_auto", payload);
            if (!success) clientsToRemove.push(sub);
        });

        this._listeners = this._listeners.filter(
            sub => clientsToRemove.indexOf(sub) == -1
        );

        await sleep(10 * this.timeout);

        this._loopSlow();

    }    

}

/**
 * Used to wait some time in async context
 * 
 * @param t time to wait
 */
function sleep (t: ms) {
    return new Promise<void>((res, _) => setTimeout(() => res(), t));
}