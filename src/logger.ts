import { cwd } from "process";
import * as fs from "fs";
import { escapeAnsiCodes } from "./core-deco";

/** Level of a log line */
export enum LogLevel {
    LOG     = "INFO",
    ERROR   = "ERROR",
    WARN    = "WARN",
    DEBUG   = "DEBUG"
}

/** A log line */
export type LogLine = {
    level: LogLevel,
    message: string,
    date: number,
};

/** A filter to retrieve specific log lines */
export type LoggerFilter = {
    /** Level of the log lines */
    level?: LogLevel,
    /** Minimum date of the log line */
    minDate?: number,
    /** Maximum date of the log line */
    maxDate?: number,
    /** The log message should contain a certain value */
    contains?: RegExp,
}

/** The parameters of the log retrieval */
export type LoggerOptions = {
    /** Max number of lines to get */
    limit?: number,
    /** Number of lines to skip */
    offset?: number,
    /** A filter */
    filter?: LoggerFilter,
};

/**
 * A standardized way to log things
 * 
 * Stores the logs both in plain text and JSON,
 * and prints them to the console with colors.
 *
 */
export class Logger {

    /** Colored log level identifier */
    static DECORATION = {
        "INFO":     "\x1b[1;92m[INFO]\x1b[0m",
        "ERROR":    "\x1b[1;31m[ERROR]\x1b[0m",
        "WARN":     "\x1b[1;33m[WARN]\x1b[0m",
        "DEBUG":    "\x1b[1;35m[DEBUG]\x1b[0m"
    };
    
    /** Log level identifier */
    static PLAIN_DECORATION = {
        "INFO":     "[INFO]",
        "ERROR":    "[ERROR]",
        "WARN":     "[WARN]",
        "DEBUG":    "[DEBUG]"
    };

    /** External listeners */
    private listeners: ((l: LogLine) => void)[] = [];

    /** The cache containing log lines */
    private logs: LogLine[] = [];

    /** The locale for the date */
    private locale: string;

    /** The timezone for the date */
    private timezone: string;

    /** The directory in which the log files are contained */
    private logDir: string;

    /** The plain text log output file */
    private logFile: string;

    /** The JSON output file */
    private jsonFile: string;

    /** The file stream for the plain text log */
    private _stream: fs.WriteStream;

    /**
     * @param locale 
     * @param timezone 
     * @param logDir 
     * @param append 
     */
    constructor (locale = "fr-FR", timezone = "Europe/Paris", logDir = cwd() + "/logs", append = false) {
        this.locale = locale;
        this.timezone = timezone;

        this.logDir = logDir;
        let logFile = this.logFile = `${logDir}/latest.log`;
        let jsonFile = this.jsonFile = `${logDir}/log_db.json`;

        // Creating the "logs" dir if non-existent
        if (!fs.existsSync(logDir)) {
            fs.mkdirSync(logDir);
        }

        // Moving the old text log file 
        if (fs.existsSync(logFile) && !append) {
            let stat = fs.lstatSync(logFile)
            let newName = `${logDir}/${this._formatDate(stat.birthtimeMs).replace(/\//g, "-")}.log`;
            fs.renameSync(logFile, newName)
        }

        // Opening the text stream
        this._stream = fs.createWriteStream(logFile, { encoding: "utf-8", flags: "a" });

        // Loading the old JSON logs
        if (fs.existsSync(jsonFile)) {
            let rawDB = fs.readFileSync(jsonFile, "utf-8");
            try {
                this.logs = JSON.parse(rawDB);
            } catch (e) {
                console.error("Can't open log database", e);
            }
        }
        
        // Write the JSON logs every 250ms
        setInterval(() => {
            fs.writeFileSync(jsonFile, JSON.stringify(this.logs, undefined, 4), { encoding: "utf-8" });
        }, 250);

    }

    /** 
     * Generates the log line by converting every argument into a single string 
     * 
     * @param level The level of the log line
     * @param args Anything to log
     */
    private _line (level: LogLevel, ...args: any[]) {

        let now = new Date();

        let line = {
            date: now.getTime(),
            level,
            message: ""
        }

        let msg = [];
        for (let arg of args) {

            switch (typeof arg) {

                case "string":
                    msg.push(arg);
                    break;

                case "object":
                    msg.push(JSON.stringify(arg, undefined, 4));
                    break;

                default:
                    msg.push(arg.toString());

            }

        }

        line.message = msg.join(' ');
        return line;

    }

    /** 
     * Generates the date prefix for the log line output 
     * 
     * @param timestamp The timestamp of the date to convert to string
     */
    private _formatDate (timestamp: number) {
        let date = new Date(timestamp);
        return `[${date.toLocaleString(this.locale, { timeZone: this.timezone })}]`;
    }

    /** 
     * Prints the log line to the terminal 
     * 
     * @param ln The log line to print
     */
    private _print (ln: LogLine) {
        let date = this._formatDate(ln.date);
        let deco = Logger.DECORATION[ln.level];
        console.log(`\x1b[1;90m${date} ${deco} ${ln.message}`);
    }

    /**
     * Writes the log line to the text log file and adds it to the internal cache
     * 
     * @param ln The line to write
     */
    private _write (ln: LogLine) {        
        let date = this._formatDate(ln.date);
        let deco = Logger.PLAIN_DECORATION[ln.level];
        let msg = escapeAnsiCodes(ln.message)
        this._stream.write(`${date} ${deco} ${msg}\n`);

        this.logs.unshift(ln);
    }

    /**
     * Logs a line with the "INFO" level
     * 
     * @param args Anything to log
     */
    log (...args: any[]) {
        let ln = this._line(LogLevel.LOG, ...args);
        this._print(ln);
        this._write(ln);
        this.listeners.forEach(l => l(ln))
    }

    /**
     * Logs a line with the "WARN" level
     * 
     * @param args Anything to log
     */
    warn (...args: any[]) {
        let ln = this._line(LogLevel.WARN, ...args);
        this._print(ln);
        this._write(ln);
        this.listeners.forEach(l => l(ln))
    }

    /**
     * Logs a line with the "ERROR" level
     * 
     * @param args Anything to log
     */
    error (...args: any[]) {
        let ln = this._line(LogLevel.ERROR, ...args);
        this._print(ln);
        this._write(ln);
        this.listeners.forEach(l => l(ln))
    }

    /**
     * Logs a line with the "DEBUG" level
     * 
     * @param args Anything to log
     */
    debug (...args: any[]) {
        let ln = this._line(LogLevel.DEBUG, ...args);
        this._print(ln);
        this._write(ln);
        this.listeners.forEach(l => l(ln))
    }

    /**
     * Adds an external log processor
     * 
     * @param callback The external hook
     */
    listen (callback: (l: LogLine) => void) {
        this.listeners.push(callback);
    }

    /**
     * Removes an external log processor
     * 
     * @param callback The external hook
     */
    detach (callback: (l: LogLine) => void) {
        this.listeners = this.listeners.filter(e => e != callback);
    }

    /**
     * Get the log lines
     * 
     * @param options Options to make a more precise search
     * @returns The log lines 
     */
    get (options: LoggerOptions = {}) {

        let out = [];
        let limit = options.limit || 100;
        let filter = options.filter || {  };
        let offset = options.offset || 0;

        let len = this.logs.length;

        for (let i = offset; i < len && out.length < limit; i++) {
            let ln = this.logs[i];
            if (conforme(ln, filter)) {
                out.unshift(ln);
            }
        }

        return out;

    }

    /**
     * Prints to the terminal the logs according to the options
     * 
     * @param options Options to make a more precise search
     */
    replay (options: LoggerOptions) {
        let lns = this.get(options);
        for (let i = 0; i < lns.length; i++) {
            this._print(lns[i]);
        }
    }

    /**
     * Creates an interface adding a prefix to the log lines
     * 
     * This is used to track which part of the code generated the log.
     * 
     * @param name Name of the prefix
     * @returns The log interface
     */
    getInterface (name: string) {
        return new LoggerInterface(name, this);
    }

}


/**
 * An interface adding a prefix to the log lines
 * 
 * This is used to track which part of the code generated the log. 
 */
export class LoggerInterface {

    constructor (private interfaceName: string, private logger: LoggerInterface | Logger) {

    }

    /** The prefix for the log lines */
    private get _name () {
        return `[${this.interfaceName}]`;
    }

    /**
     * Logs a line with the "INFO" level
     * 
     * @param args Anything to log
     */
    log (...args: any[]) {
        this.logger.log(this._name, ...args);
    }

    /**
     * Logs a line with the "ERROR" level
     * 
     * @param args Anything to log
     */
    error (...args: any[]) {
        this.logger.error(this._name, ...args);
    }

    /**
     * Logs a line with the "WARN" level
     * 
     * @param args Anything to log
     */
    warn (...args: any[]) {
        this.logger.warn(this._name, ...args);
    }

    /**
     * Logs a line with the "DEBUG" level
     * 
     * @param args Anything to log
     */
    debug (...args: any[]) {
        this.logger.debug(this._name, ...args);
    }

    /**
     * Creates an interface adding a prefix to the log lines
     * 
     * This is used to track which part of the code generated the log.
     * 
     * @param name Name of the prefix
     * @returns The log interface
     */
    getInterface (name: string) {
        return new LoggerInterface(name, this);
    }

}

/**
 * Checks if a log line complies to a filter
 * 
 * @param ln The log line 
 * @param filter The filter to compare with
 * @returns The log line is compliant
 */
function conforme (ln: LogLine, filter: LoggerFilter): boolean { 

    return ( !filter.contains || !!ln.message.match(filter.contains) )  // Checks if the message contains a specific pattern
        && ( !filter.level || ln.level == filter.level )                // Checks if the log level is the one required
        && ( !filter.maxDate || ln.date <= filter.maxDate )             // Checks if the date is under the max limit
        && ( !filter.minDate || ln.date >= filter.minDate );            // Checks if the date is over the min limit
        
}

