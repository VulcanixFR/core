import { argv, exit } from "process";
import { LogLevel, Logger, LoggerFilter, LoggerOptions } from "../logger";

// Getting the args
let [ exe, script, ...args ] = argv;

// Prints the help message
function help () {
    console.log(`${exe} ${script} [...flags]`);
    console.log("Prints the latest core logs according to flags\n");
    [
        "-c, --contains:    RegExp to filter by log messages",
        "-f, --from:        Limit date on the older logs",
        "-h, --help:        Shows this message and quit",
        "-l, --level:       Filters by log level (INFO, WARN, ERROR, DEBUG)",
        "-n, --number:      Limits the number of log lines shown",
        "-o, --offset:      Number of log lines to skip",
        "-t, --to:          Limit date on the newer logs"
    ].forEach(e => console.log("  " + e));
}

// Setting up the values to configure
let filter: LoggerFilter = {

};
let options: LoggerOptions = {
    filter
};

// Parsing the command line
for (let i = 0; i < args.length; i += 2) {

    let flag = args[i];
    let arg = args[i + 1];

    switch (flag) {

        case "-n":
        case "--number":
            options.limit = parseInt(arg);
            break;

        case "-o":
        case "--offset":
            options.offset = parseInt(arg);
            break;

        case "-l":
        case "--level":
            filter.level = <LogLevel>arg;
            break;

        case "-f":
        case "--from":
            filter.minDate = (new Date(arg)).getTime();
            break;

        case "-t":
        case "--to":
            filter.maxDate = (new Date(arg)).getTime();
            break;

        case "-c":
        case "--contains":
            filter.contains = new RegExp(arg);
            break;

        case "-h":
        case "--help":
            help();
            exit(0);

        default:
            console.error("Unknown flag", flag);
            exit(1);

    }

}

let logger = new Logger("fr-FR", "Europe/Paris", "logs", true);

logger.replay(options);
exit(0);