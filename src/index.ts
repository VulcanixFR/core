import { Cockpit } from "./cockpit/cockpit";
import { SystemVitals } from "./systemVitals";
import { initUnixServer } from "./unixSocket";
import { vitalsRoute } from "./cockpit/routes/vitals";
import { subprocessRoute } from "./cockpit/routes/subprocess";
import { Logger } from "./logger";
import { asciiLogo, logo2024, mergeLogos } from "./core-deco";
import { ServerUDPDescription, initUDPDiscovery } from "./udpDiscovery";
import { initTCPServer } from "./tcpSocket";
import { initWSServer } from "./webScoket";
import { Subprocess, SubprocessManager } from "./subprocessManager";
import { config } from "dotenv";
import { basename, dirname } from "path";
import { ChildProcessWithoutNullStreams } from "child_process";

// Initializing Cockpit's dependencies
const vitals = new SystemVitals();
const logger = new Logger();
const manager = new SubprocessManager(logger.getInterface("Subprocess"));

// Printing the Welcome screen
const welcomeScreen = mergeLogos(logo2024(), asciiLogo("0.0.1")).logo.join('\r\n'); // Merging this year's logo with Core's logo
logger.log("\n" + welcomeScreen);

// Instanciating Cockpit
const cockpit = new Cockpit(logger, vitals, manager);

// Attaching sub routes
cockpit.use("vitals", vitalsRoute());
cockpit.use("subprocess", subprocessRoute());

// Opening the Unix Socket
const unix = initUnixServer("_sock", cockpit);

// Opening the TCP Socket
const TCP = initTCPServer(12001, cockpit);

// Opening the WS Server
const ws = initWSServer(12002, cockpit);

// Starting UDP discovery server
let d: ServerUDPDescription = {
    id: "server",
    description: "Should be defined in a config file",
    color: { r: 4, g: 147, b: 255 }
};
const udp = initUDPDiscovery(d, logger.getInterface('UDP'));



// Some commands for Cockpit's root
cockpit.cmd("logo", (req) => req.respond("\x1b[0m" + welcomeScreen, "logo"), { args: [], description: "Displays the program's logo" });


// Test - Attaches Core to Core Manager
import readline from "node:readline/promises";
config();
let corePath = process.env["CORE"];
if (corePath) {

    logger.log("Trying to start Core");

    let exe = "./" + basename(corePath);
    let workDir = dirname(corePath);

    let attachProc = (proc: ChildProcessWithoutNullStreams) => {

        proc.stdin.end();
        let rl = readline.createInterface(proc.stdout);

        rl.on("line", ln => {
            let o = JSON.parse(ln);
            logger.debug("Core: ", o);
        });

    };

    let sub = manager.addService(exe, [], workDir, proc => {
        if (!proc.process) {
            logger.error("Wierd restart bug @ Core");
            return;
        }
        attachProc(proc.process);
    });

    sub.start()
        .catch(err => logger.error("Failed to start Core: " + err.message))
        .then((proc) => {
            if (!proc) {
                logger.error("Wierd start bug @ Core");
                return;
            }
            attachProc(proc);
        });

}
