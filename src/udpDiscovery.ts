import { createSocket } from "dgram";
import { LoggerInterface } from "./logger";

/**
 * THIS CODE IS A PROTOTYPE
 * 
 * The goal is to make the Core's hosts discoverable using UDP broadcast
 * on the subnet on which each machine is connected to.
 * 
 * A nice to have feature, not critical.
 */

const CoreUDPPort = 42012;

export type ServerUDPDescription = {
    id: string,
    description: string,
    color: { r: number, g: number, b: number }
}

export function initUDPDiscovery (description: ServerUDPDescription, logger: LoggerInterface) {

    let socket = createSocket("udp4");

    socket.on("listening", () => {
        logger.log("Server listening on port", CoreUDPPort);
    });

    socket.on("message", (msg, remote) => {

        logger.debug(`Recieved ${msg} from ${remote.address}:${remote.port}`);
        let payload = Buffer.from(JSON.stringify(description));

        socket.send(payload, remote.port, remote.address);

    });

    socket.bind(CoreUDPPort);

    return socket;

}