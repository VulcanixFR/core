import { ChildProcessWithoutNullStreams, spawn } from "child_process"
import { cwd } from "process";
import { LoggerInterface } from "./logger";

/**
 * Run command return type
 */
export type RunnerReturns = {
    /** All of the content written in stdout */
    stdout: string, 
    /** All of the content written in stderr */
    stderr: string  
}

/**
 * 
 */
export class SubprocessManager {

    processes: Subprocess[] = [];
    
    constructor (private logger: LoggerInterface) {



    }

    /**
     * Tries to find a subprocess with a matching ID
     * @param id The id we're looking for
     * @returns The subprocess or undefined
     */
    getSubprocess (id: string) {
        return this.processes.filter(e => e.id == id)[0];
    }

    /**
     * Starts a program and returns the outpunt when closed
     * 
     * @param exe The name of the executable
     * @param args The args to spawn the executable
     * @param workDir The working directory to spawn in
     * @returns The output of the program
     */
    run (
        exe: string, args: string[], workDir?: string
    ): Promise<RunnerReturns> {

        return new Promise((resolve, reject) => {
            
            let stdout: string = "";
            let stderr: string = "";
            let id = "run:" + exe;

            let exitHandler = (code: number) => {
                if (code == 0) {
                    resolve({ stderr, stdout });
                } else {
                    this.logger.error(
                        `Process ${id} exited with non-zero return code: ${code}`
                    );
                    let temp = new Error(
                        `Process exited with non-zero return code: ${code}\r\n`
                        + "STDOUT:\r\n" + stdout
                        + "STDERR:\r\n" + stderr
                    )
                    reject(temp);
                }
            };
            
            let proc = this.getSubprocess(id);
            this.logger.debug("Creating new runner: " + exe);
            
            if (!proc) {
                proc = new Subprocess(id, exe, args, { workDir });
                this.processes.push(proc);
            } 

            else if (proc.running) {
                this.logger.debug(`${id} already running !`);
                reject(new Error(`${id} already running !`));
                return;
            }

            else {
                proc.exe = exe;
                proc.args = args;
                proc.workDir = workDir || cwd();
            }

            proc.handleProcessClose = c => exitHandler(c);

            proc.start()
                .then(process => {
                    this.logger.debug("Successfully started " + exe);
                    process.stdout.on("data", data => {
                        stdout += data.toString();
                    });
                    process.stderr.on("data", data => {
                        stderr += data.toString();
                    });
                    process.stdin.end();
                })
                .catch(err => {
                    this.logger.error(
                        `Failed to start runner ${exe}: ${err.message}`
                    );
                    reject(err)
                });

        });

    }

    /**
     * Adds a service in the subprocess list
     * 
     * @param exe Executable to run
     * @param args Arguments for the executable
     * @param workDir Directory in which to execute
     * @param onRestart Callback when the service restarts after an error
     * 
     * @returns The subprocess
     */
    addService (
        exe: string, args: string[], workDir: string, 
        onRestart: (proc: Subprocess) => void
    ): Subprocess {

        let id = "service:" + exe
        let temp = this.getSubprocess(id);
        if (temp) {
            this.logger.error("Tried to add existing service " + id);
            return temp;
        }

        temp = new Subprocess(id, exe, args, { workDir, type: "service" });

        temp.handleProcessClose = code => {
            if (code != 0) {
                this.logger.error(`Service ${temp.id} exited with non-zero code`);
                // The service crashed, we try to start it up again
                temp.restart().catch(err => {
                    this.logger.error(`Service ${temp.id} failed to restart`);
                }).then(() => onRestart(temp));
            }
        }

        this.processes.push(temp);
        this.logger.debug("Added new service " + id);

        return temp;

    }

    /**
     * Removes a service form the subprocess list.
     * The service must be stopped beforehands.
     * 
     * @param exe The executable name used by the service
     * @returns true If the service is removed
     * @returns false If the service is still running
     */
    rmService (
        id: string
    ) {
        
        let temp = this.getSubprocess(id);
        if (!temp) return true;

        if (temp.running) return false;

        this.processes = this.processes.filter(e => e != temp);
        this.logger.debug("Removed service " + temp.id);

        return true;
    }

    /** Returns the list of subprocesses and thier status */
    get list () {

        let runners = this.processes.filter(
                p => p.type == "runner"
            ).map(p => p.status);
        
        let services = this.processes.filter(
                p => p.type == "service"
            ).map(p => p.status);

        return {
            runners, services
        }
    }

}

/**
 * Options used to spawn a subprocess
 */
export type SubprocessOptions = {
    type?: 'service' | 'runner';
    workDir?: string;
    maxFailCount?: number;
};

/**
 * A description of the Subprocess status
 */
export type SubprocessStatus = {
       
    id: string;
    type: 'service' | 'runner';

    exe: string;
    args: string[];
    workDir: string;
    
    created: Date;
    started: Date;
    ended: Date;

    fails: number;
    failsSinceLastSuccess: number;
    maxFailCount: number;

    running: boolean;

}
/**
 * A wrapper for Node.JS's child process
*/
export class Subprocess {
    
    public id: string;
    public type: 'service' | 'runner';

    public exe: string;
    public args: string[];
    public workDir: string;
    
    public created: Date = new Date();
    public started: Date;
    public ended: Date;

    public fails: number = 0;
    public failsSinceLastSuccess: number = 0;
    public maxFailCount: number;

    public process?: ChildProcessWithoutNullStreams;

    public handleProcessClose: (code: number) => void = () => {};

    /**
     * A wrapper for Node.JS's child process
     * 
     * @param id The ID of the subprocess
     * @param exe The name of the executable
     * @param args The arguments to spawn the executable
     * @param options The options for the subprocess
     */
    constructor (
        id: string, 
        exe: string, 
        args: string[], 
        options?: SubprocessOptions
    ) {

        this.id = id;
        this.type = options?.type || 'runner';

        this.exe = exe;
        this.args = args;

        this.workDir = options?.workDir || cwd();

        this.started = this.created;
        this.ended = this.created;

        this.maxFailCount = options?.maxFailCount || 3;

    }

    /**
     * Tries to spawn the child process
     * 
     * @returns The child process if successfull
     */
    start (): Promise<ChildProcessWithoutNullStreams> {

        return new Promise((resolve, reject) => {
    
            this.process = spawn(this.exe, this.args, { 
                cwd: this.workDir
            });
                
            this.process.on("error", (err) => {
                let temp = new Error(
                    `Spawn failed of command: ${this.exe} ${
                        JSON.stringify(this.args)
                    } :\r\n` + JSON.stringify(err)
                );
    
                reject(temp);
    
            });
    
            this.process.on("close", (code) => {
                this.ended = new Date();
                this.handleProcessClose(code || 0);
            });
    
            this.process.on("spawn", () => {
                if (!this.process) {
                    let temp = new Error("Wierd error that should never be");
                    reject(temp);
                    return;
                }
                this.started = new Date();
                this.failsSinceLastSuccess = 0;
                resolve(this.process);
            })

        });

    }

    /**
     * Tries to spawn the child_process again after a failed attempt
     * 
     * @returns The child process if successfully spawned
     */
    restart (): Promise<ChildProcessWithoutNullStreams> {

        return new Promise((resolve, reject) => {

            // We increment the fail count
            this.failsSinceLastSuccess++;
            this.fails++;

            if (this.failsSinceLastSuccess >= this.maxFailCount) {
                // We fail too much
                let temp = new Error(
                    `Spawning ${this.id} failed too much (${this.maxFailCount})`
                );
                reject(temp);
                return;
            }

            // We try to start again
            this.start().catch(err => reject(err)).then(
                proc => {
                    if (!proc) {
                        let temp = new Error("Void promise");
                        reject(temp);
                        return;
                    }
                    resolve(proc);
                }
            );

        });

    }

    /** Resets the fail count to allow restarts */ 
    reset () {
        this.failsSinceLastSuccess = 0;
    }

    /**
     * Is the process running ?
     */
    get running () {
        if (!this.process) return false;
        return this.process.exitCode == null && !this.process.killed;
    }

    /**
     * The process status
     */
    get status (): SubprocessStatus {
        return ({
            args: this.args,
            created: this.created,
            ended: this.ended,
            exe: this.exe,
            fails: this.fails,
            failsSinceLastSuccess: this.failsSinceLastSuccess,
            id: this.id,
            maxFailCount: this.maxFailCount,
            started: this.started,
            type: this.type,
            workDir: this.workDir,
            running: this.running
        });
    }

}