import { WebSocketServer } from "ws";
import { Cockpit } from "./cockpit/cockpit";
import { CockpitWSClient } from "./cockpit/wsClient";

/**
 * Creates a WebSocket server 
 * @param port Port to listen to
 * @returns The server
 */
export function initWSServer (port: number, cockpit: Cockpit): WebSocketServer {

    let srv = new WebSocketServer({ port });
    let logger = cockpit.logger.getInterface("WebSocket Server");

    // Code to log when the socket is opened
    srv.on("listening", () => {
        logger.log(`WebSocket opened on port \x1b[1;35m${port}\x1b[0m`);
    })

    // Code to handle the new Unix Socket Clients
    srv.on("connection", sock => {

        let client = new CockpitWSClient(sock);
        cockpit.register(client);

    });
    
    return srv;

}