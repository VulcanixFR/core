import { existsSync, unlinkSync } from "fs";
import { Server } from "net";
import { Cockpit } from "./cockpit/cockpit";
import { CockpitSocketClient } from "./cockpit/socketClient";

/**
 * Creates a Unix Socket server 
 * @param path Path to the unix socket
 * @returns The server
 */
export function initUnixServer (path: string, cockpit: Cockpit): Server {

    let srv = new Server();
    let logger = cockpit.logger.getInterface("Unix Socket Server");
    if (existsSync(path)) unlinkSync(path);

    // Code to log when the socket is opened
    srv.on("listening", () => {
        logger.log(`Unix Socket opened at \x1b[1;35m${path}\x1b[0m`);
    })

    // Code to handle the new Unix Socket Clients
    srv.on("connection", sock => {

        let client = new CockpitSocketClient(sock, "unix");
        cockpit.register(client);

    });

    // Opens the socket
    srv.listen(path);
    
    return srv;

}