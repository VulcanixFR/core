import { CockpitRequest } from "../cockpit";
import { CockpitRoute, HandlerHelp } from "../route";

/**
 * Creates and return the route for vitals related commands
 * 
 * @returns The route for vitals related commands
 */
export function vitalsRoute (): CockpitRoute {

    let route: CockpitRoute = new CockpitRoute("Vitals", "Get the system status");

    route.cmd("get", get, getHelp);
    route.cmd("sub", sub, subHelp);
    route.cmd("unsub", unsub, unsubHelp);

    return route;

}

/**
 * Processes the `vitals get` command, 
 * will send back to the client the system vitals.
 * 
 * The packet sent will be of type `vitals`
 * 
 * @param req The request
 */
async function get (req: CockpitRequest) {

    let status = await req.cockpit.vitals.latestVitals();
    req.respond(JSON.stringify(status), "vitals");

}

/**
 * Mandatory command declaration for the get command
 */
const getHelp: HandlerHelp = {
    args: [],
    description: "Get the instant status of the system"
}

/**
 * Processes the `vitals sub` command,
 * will send a packet each time the vitals are refreshed.
 * 
 * The packet sent on subscription will be of type `ok`
 * 
 * The packet sent on each refresh will be of type `vitals_auto`
 * 
 * @param req The request
 */
async function sub (req: CockpitRequest) {

    req.cockpit.vitals.subscribe(req.client);
    req.respond("Subscription to system vitals registered");

}

/**
 * Mandatory command declaration for the sub command
 */
const subHelp: HandlerHelp = {
    args: [],
    description: "Subscribes to the system vitals"
}

/**
 * Processes the `vitals unsub` command,
 * will stop sending a packet each time the vitals are refreshed.
 * 
 * The packet sent on cancel will be of type `ok`
 * 
 * @param req The request
 */
async function unsub (req: CockpitRequest) {
    
    req.cockpit.vitals.unsubscribe(req.client);
    req.respond("Subscription to system vitals cancelled");

}

/**
 * Mandatory command declaration for the unsub command
 */
const unsubHelp: HandlerHelp = {
    args: [],
    description: "Cancels subscription to the system vitals"
}