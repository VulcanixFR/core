import { CockpitRequest } from "../cockpit";
import { COCKPIT_ERR } from "../errors";
import { CockpitRoute, HandlerHelp } from "../route";

export function subprocessRoute (): CockpitRoute {
    
    let route = new CockpitRoute("Subprocess", "Manage servicies and runners");

    route.cmd("run", run, runHelp);
    route.cmd("list", list, listHelp);
    route.cmd("add", add, addHelp);
    route.cmd("rm", rm, rmHelp);
    route.cmd("start", start, startHelp);
    route.cmd("stop", stop, stopHelp);

    return route;

}

/**
 * Processes the "run" request.
 * 
 * The packet sent will be of type `runner_result`
 * 
 * @param req The request
 */
function run (req: CockpitRequest) {

    let workDir = <string>req.command.shift();
    let exe = <string>req.command.shift();
    let args = req.command;

    let manager = req.cockpit.subprocess;

    manager.run(exe, args, workDir)
        .then(output => {
            req.respond(JSON.stringify({
                command: { exe, args, workDir },
                stdout: output.stdout,
                stderr: output.stderr
            }), "runner_result");
        })
        .catch((err: Error) => {
            req.error(err.message, COCKPIT_ERR.unknown);
        });

}

/**
 * Mandatory command declaration for the run command
 */
const runHelp: HandlerHelp = {
    description: "Runs a command in a specific work directory",
    args: [ 
        { name: "workDir", required: true },
        { name: "exe", required: true },
        { name: "...args", required: false }
    ]
}

/**
 * Processes the "list" request.
 * 
 * The packet sent will be of type `subprocess_list`
 * 
 * @param req The request
 */
function list (req: CockpitRequest) {
    
    req.respond(
        JSON.stringify(req.cockpit.subprocess.list),
        "subprocess_list"
    );

}

/**
 * Mandatory command declaration for the list command
 */
const listHelp: HandlerHelp = {
    args: [],
    description: "Lists all subprocesses"
}

/**
 * Processes the "add" request.
 * 
 * @param req The request
 */
function add (req: CockpitRequest) {
    
    let workDir = <string>req.command.shift();
    let exe = <string>req.command.shift();
    let args = req.command;

    let manager = req.cockpit.subprocess;

    let s = manager.addService(exe, args, workDir, () => {});

    req.respond(s.id);

}

/**
 * Mandatory command declaration for the add command
 */
const addHelp: HandlerHelp = {
    args: [
        { name: "workDir", required: true },
        { name: "exe", required: true },
        { name: "...args", required: false },
    ],
    description: "Adds a service"
}

/**
 * Processes the "rm" request.
 * 
 * @param req The request
 */
function rm (req: CockpitRequest) {
    
    let id = <string>req.command.shift();
    let manager = req.cockpit.subprocess;

    if (manager.rmService(id)) {
        req.respond(id);
    } else {
        req.error("Service still running");
    }


}

/**
 * Mandatory command declaration for the rm command
 */
const rmHelp: HandlerHelp = {
    args: [
        { name: "service", required: true }
    ],
    description: "Removes a service"
}

/**
 * Processes the "start" request.
 * 
 * @param req The request
 */
function start (req: CockpitRequest) {
    
    let id = <string>req.command.shift();
    let manager = req.cockpit.subprocess;

    let srv = manager.getSubprocess(id);

    if (!srv) {
        req.error(`Service ${id} not found`);
        return;
    }

    if (srv.running) {
        req.error(`Service ${id} aleready started`);
        return;        
    }

    srv.start().catch(err => {
        req.error(err.message);
    }).then(process => {
        if (!process) {
            req.error("Wierd start error");
            return;
        }
        req.respond(`${process.pid}`);
    });

}

/**
 * Mandatory command declaration for the rm command
 */
const startHelp: HandlerHelp = {
    args: [
        { name: "service", required: true }
    ],
    description: "Starts a service"
}

/**
 * Processes the "stop" request.
 * 
 * @param req The request
 */
function stop (req: CockpitRequest) {
    
    let id = <string>req.command.shift();
    let manager = req.cockpit.subprocess;

    let srv = manager.getSubprocess(id);

    if (!srv) {
        req.error(`Service ${id} not found`);
        return;
    }

    if (!srv.running) {
        req.error(`Service ${id} aleready stopped`);
        return;        
    }

    let proc = srv.process;
    if (!proc) {
        req.error(`Service ${id} never started`);
        return;        
    }

    console.log(proc);

    proc.kill('SIGTERM');
    
    req.respond(id);

}

/**
 * Mandatory command declaration for the stop command
 */
const stopHelp: HandlerHelp = {
    args: [
        { name: "service", required: true }
    ],
    description: "Stops a service"
}