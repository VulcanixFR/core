/**
 * All available Cockpit error codes
 */
export enum COCKPIT_ERR {

    // Process Errors (Starting @ 0)
    unknown             = 0,
    invalidBufferType   = 1,
    invalidJSONObject   = 2,
    invalidPacket       = 3,

    // Request Errors (Starting @ 100)
    commandNotFound     = 100,
    toFewArguments      = 101,
    toManyArguments     = 102,
    notYetImplemented   = 103,

};

/**
 * Contains the default error messages
 */
export const COCKPIT_ERR_MESSAGE = {

    // Process Errors (Starting @ 0)
    0: "Unknown error",
    1: "Invalid buffer type",
    2: "Invalid JSON object",
    3: "Invalid packet",

    // Request Errors (Starting @ 100)
    100: "Command not found",
    101: "To few arguments",
    102: "To many arguments",
    103: "Not yet implemented",

}