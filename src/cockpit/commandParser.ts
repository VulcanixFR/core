
/**
 * Decomposes a command in a bash-like fashion
 * @param cmd The command to parse
 * @returns The command and its arguments
 */
export function parseCommand (cmd: string): string[] {

    // Flags
    let backslash = false;
    let inQuote = false;
    let inBit = false;

    // Last opened quote type
    let quoteType = "";

    // The buffer
    let bit = "";

    // The output
    let out: string[] = [ ];

    for (let i = 0; i < cmd.length; i++) {

        // Get the next char
        let char = cmd.charAt(i);
        
        // Checks if we have to escape the next 
        if (!backslash) {

            // If we have a \, we escape the next char
            if (char == "\\") {
                backslash = true;
                continue;
            } 
            
            // If we have a quote, we escape until the next similar quote
            else if (!(inBit || inQuote) && (char == "'" || char == '"')) {
                inQuote = true;
                quoteType = char;
                continue;
            }
                
            // If we have a whitespace char, we push the current buffer into the output list
            else if (!inQuote && char.match(/[\ \r\n\t]/)) {
                if (inBit) {
                    inBit = false;
                    out.push(bit);
                    bit = "";
                }
                continue;
            }

            // Else we are in a word
            else if (!inQuote) {
                inBit = true;
            }

        }


        if (!backslash && inQuote && quoteType == char) {
            // We found an unescaped similar quote while beeing inside of a string
            // We push the buffer in the output and go on to the next char
            inQuote = false;
            out.push(bit);
            bit = "";
            continue;
        }

        if (backslash) {
            // We escaped the char, lower the flag
            backslash = false;
        }

        // We add the current chat in the buffer
        bit += char;

    }

    // If the buffer is not empty, we push it into the output
    if (bit != "") out.push(bit);

    return out;

}