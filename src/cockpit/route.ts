import { CockpitRequest } from "./cockpit";
import { COCKPIT_ERR } from "./errors";

/**
 * Callback involving a request used by routes
 */
export type CockpitRouteCallback = (req: CockpitRequest) => void;

/** Structure to store routes */
type Route = {
    /**
     * Route identifier
     */
    id: string,
    /**
     * Type of route
     */
    type: 'cmd',
    /**
     * Callback used to handle the command
     */
    handle: CockpitRouteCallback,
    /**
     * Description of the command use
     */
    description: string,
    /**
     * Arguments for the command
     */
    args: { required?: boolean, name: string }[]
} | {
    /**
     * Route identifier
     */
    id: string,
    /**
     * Type of route
     */
    type: 'route',
    /**
     * The sub-route who has to process command starting by it's id
     */
    handler: CockpitRoute
};

/** Structure of an help message's line */
export type HelpLine = {
    /**
     * Identifier of the route
     */
    id: string,
    /**
     * Description of the route
     */
    description: string,
    /**
     * Arguments for the command
     */
    args: { required?: boolean, name: string }[],
    /**
     * Type of route
     */
    type: 'cmd' | 'route',
};

/** Structure of an help message */
export type HelpMessage = {
    /** Title of the help message */
    title: string,
    /** Description of the help message root */
    descritpion: string,
    /** Lines of the help message */
    lines: HelpLine[],
}

/** Structure of an help line for a route handler */
export type HandlerHelp = {
    /** Description of the command use */
    description: string,
    /** Arguments for the command */
    args: { required?: boolean, name: string }[]
}

/** 
 * Route that represents the commands and subcommands 
 * 
 * Greatly inspired by express.js
 * */
export class CockpitRoute {

    private _routes: Route[] = [];

    constructor (readonly title: string, readonly description: string) {

        this.cmd("help", req => this.help(req), { description: "Shows this help message", args: [] })

    }

    /**
     * Tries to find the route to fullfill the request
     * @param req The request
     */
    handle (req: CockpitRequest): void {

        let id = req.command.shift();
        if (!id) {
            this.help(req);
            return;
        }

        // Tries to find the route with a matching id
        let r: Route | undefined = this._routes.filter(e => e.id == id)[0];

        // Route not found
        if (!r) {
            req.error(id, COCKPIT_ERR.commandNotFound);
            this.help(req);
            return;
        }

        // Checking if it's a nested router or a command
        if (r.type == "route") {
            r.handler.handle(req);
        } else {

            // Check required args
            let reqArgsLen = r.args.filter(e => e.required).length;
            if (reqArgsLen > req.command.length) {
                req.error("", COCKPIT_ERR.toFewArguments);
                this.help(req, r);
                return;
            }

            r.handle(req);

        }

    }

    /**
     * Generates the help message of this route
     * @param req The requests needing the message
     * @param route Filters the message to only one route
     */
    help (req: CockpitRequest, route?: Route) {

        let toList = this._routes;
        if (route) toList = toList.filter(e => e == route);

        let lines: HelpLine[] = toList.map(e => e.type == "cmd" ? ({
            args: e.args, description: e.description, id: e.id, type: e.type
        }) : ({
            args: [], description: e.handler.description, id: e.id, type: e.type
        }));

        let message: HelpMessage = {
            title: this.title,
            descritpion: this.description,
            lines
        };
        
        let payload = JSON.stringify(message);

        req.respond(payload, "help");

    }

    /**
     * Adds a command to this route
     * @param id The command
     * @param handler The callback
     * @param help The help line
     */
    cmd (id: string, handler: CockpitRouteCallback, help: HandlerHelp) {

        this._routes.push({
            type: "cmd",            
            id,
            args: help.args,
            description: help.description,
            handle: handler,
        });

    }

    /**
     * Adds a sub route to this route
     * @param id The route "command"
     * @param route The route
     */
    use (id: string, route: CockpitRoute) {

        this._routes.push({
            type: "route",
            id,
            handler: route,
        });
        
    }

}