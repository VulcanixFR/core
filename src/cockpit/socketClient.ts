import { Socket } from "net";
import { CockpitPacket, CockpitClient, isCockpitPacket } from "./client";
import { COCKPIT_ERR } from "./errors";

let id = 0;

/**
 * Cockpit Client implementation for UNIX and TCP sockets
 */
export class CockpitSocketClient extends CockpitClient {

    constructor (private socket: Socket, type: "unix" | "tcp") {

        super();

        // Initializing the Client's type
        this.type = type;
        this.id = (id++).toString();

        // Handling socket close event
        socket.on("close", () => {
            this.onclose();
        });

        // Handling socket data event
        socket.on("data", data => {
            this.handleBuffer(data);
        });

        socket.on("error", (err) => {
            // Silent error O_O
        })

    }

    protected sendMessage(data: string): boolean {
        
        // Checks if the socket is closed
        if (!this.socket.writable) return false;

        // When in "normal" mode, a null character is added to prevent collision
        // When in "raw" mode, a chevron is added to make the message
        // look like a shell
        let suffix = this.raw ? "\r\n> " : "\0";

        // Tries to write to the socket
        try {
            this.socket.write(data + suffix);
        } catch (e) {
            return false;
        }

        return true;

    }

}