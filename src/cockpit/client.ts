import { DEFAULT_SERIALIZERS, Serializer } from "./serializers";
import { COCKPIT_ERR, COCKPIT_ERR_MESSAGE } from "./errors";

/**
 * Packet used in communication
 */
export type CockpitPacket = {

    /**
     * Unique identifier of a request, thus optionnal
     */
    uid?: string,

    /**
     * Type of packet
     */
    type: string,

    /**
     * Payload of the packet
     */
    payload: string,

    /**
     * Client identifier
     */
    client: string,

};

/**
 * Checks if the object is a CockpitPacket
 * 
 * @param obj The object to test
 * @returns The packet if valid, else a string describing the error
 */
export function isCockpitPacket (obj: Object): CockpitPacket | string {

    if ("uid" in obj && typeof obj["uid"] != "string") {
        return "uid must be a string";
    }

    if (!("client" in obj)) {
        return "Missing property: client";
    } else if (typeof obj["client"] != "string") {
        return "client must be a string";
    }

    if (!("type" in obj)) {
        return "Missing property: type";
    } else if (typeof obj["type"] != "string") {
        return "type must be a string";
    }

    if (!("payload" in obj)) {
        return "Missing property: payload";
    } else if (typeof obj["payload"] != "string") {
        return "payload must be a string";
    }

    return <CockpitPacket>obj;

}

/**
 * Possible type of client (for IntelliSense)
 */
export type CockpitClientType = 'none' | 'unix' | 'tcp' | 'ws';

/**
 * The base class for Cockpit Client.
 * 
 * To extend it, you just need to provide the `sendMessage` function 
 * to communicate over the protocol choosen and call the 
 * proper callback functions.
 */
export class CockpitClient {

    // Client ID Base
    private _id: string = "unknown";

    // Client Type parameters
    raw: boolean = false;               // Is the client expecting raw text instead of JSON ?
    type: CockpitClientType = "none";   // Type of protocol used 

    serializers: Serializer[] = DEFAULT_SERIALIZERS;    // Serializers used for raw text communication

    constructor () {

    }

    /** 
     * This callback must be called when the communication ends
     */
    onclose: () => void = () => {};

    /** 
     * This callback must be called when recieving a packet 
     * 
     * @param incoming The incoming packet
     */
    onmessage: (incoming: CockpitPacket) => void = () => {};

    /** 
     * This callback is automatically called when changing the ID 
     * 
     * @param change The object containing the old and new IDs
     */
    onIDChange: (change: {old: string, id: string}) => void = () => {};


    /** 
     * Sends a packet using `sendMessage` after serializing it 
     * 
     * @param outgoing The packet to send
     * @returns `true` if the message is sent, else `false` 
     */
    send (outgoing: CockpitPacket): boolean {
        if (this.raw) {
            return this.sendMessage(this.serialize(outgoing));
        } else {
            return this.sendMessage(JSON.stringify(outgoing));
        }
    };

    /**
     * Wraps a payload using the type and uid parameters (and the Client's ID)
     * before sending it
     * 
     * @param type The type of packet
     * @param payload The payload
     * @param uid The ID of the request of which this is a response
     * @returns `true` if the message is sent, else `false`
     */
    sendPayload (type: string, payload: string, uid?: string): boolean {
        let packet: CockpitPacket = {
            type, payload, uid, client: this.id
        };
        return this.send(packet);
    }

    /**
     * Sends a string to the client 
     * 
     * @param data The data to send to the client
     * @returns `true` if the message is sent, else `false`
     */
    protected sendMessage (data: string): boolean {
        console.log(`$ > ${this.id} : ${data}`);
        return true;
    }

    /**
     * Serializes a packet to text format if possible, used when the client
     * wants to avoid communicating using json
     * 
     * @param packet The packet to serialize
     * @returns The serialized packet
     */
    serialize (packet: CockpitPacket): string {

        // Looking for the proper function to serialize the packet
        for (let s of this.serializers) {
            if (s.id == packet.type) {
                return s.f(packet);
            }
        }

        // Default serialization
        let line = `[${packet.type}]${packet.uid ? ` (${packet.uid})` : ""}\r\n${packet.payload}`;
        return line;

    }

    /**
     * Handles recieving a buffer from a socket
     * @param data The buffer to decode
     */
    protected handleBuffer (data: Buffer): void {
        
        let raw: string;
        let packets: CockpitPacket[] = [];

        // Tries to get an utf-8 string from the recieved buffer
        try {
            raw = data.toString("utf-8");
        } catch (e) {
            this.error(
                "Failed to parse buffer as utf-8", 
                COCKPIT_ERR.invalidBufferType
            );
            return;
        }

        // The messages are separated by a null character to prevent 
        // collisions, here we separate them if they collide
        // (The filter excludes any empty string using JS magic)
        let bits = raw.split("\0").filter(e => e);

        // We try to parse each message
        for (let b of bits) {

            // If it starts by `@`, it's a signal
            if (b.startsWith("@")) {
                this.handleSignal(b);
                continue;
            }

            // We will try to convert the string to JSON
            let j: CockpitPacket;
            
            if (this.raw) {
                
                // If the client is raw, then he only sends request commands
                j = {
                    client: this.id,
                    payload: b,
                    type: "request"
                };

            } else {

                // Tries to parse the string as JSON
                try {
                    let obj = JSON.parse(b);
                    let tmp = isCockpitPacket(obj);
                    if (typeof tmp == "string") {
                        this.error(tmp, COCKPIT_ERR.invalidPacket);
                        continue;
                    }

                    j = tmp;
                    j.client = this.id; // Overriding the ID with the one we define
                    
                } catch (e) {
                    // In case of failure, an error is sent to the client                    
                    this.error(
                        "Failed to parse message as JSON", 
                        COCKPIT_ERR.invalidJSONObject
                    );
                    continue;
                }

            }

            packets.push(j);

        }

        // We process each of the recieved packets
        packets.forEach(e => this.onmessage(e));

    }

    /**
     * Processes a signal (raw text message starting with a `@` symbol), of 
     * which the arguments are separated using semicolons
     * 
     * The existing signals are:
     * - `raw`: switches this client to not use JSON if possible
     * - `json`: switches this client to use exclusively JSON
     * - `id;<new_id>`: changes this client's base ID to "new_id"
     * 
     * @param signal The signal to handle
     */
    protected handleSignal (signal: string) {

        // Removes the @ symbol and splits around ;
        let args = signal.substring(1).split(";").map(e => e.replace(/\W/, ""));
        let s = args.shift();

        switch (s) {

            case "raw":
                this.raw = true;
                this.sendPayload("ok", "Switched to raw communication");
                return;

            case "json":
                this.raw = false;
                this.sendPayload("ok", "Switched to json communication");
                return;

            case "id":
                if (args.length < 1) {
                    this.sendPayload("undefined_error", "Missing argument for signal: " + s);
                    return;
                }

                this.id = args[0];
                this.sendPayload("ok", "ID set to: " + this.id);
                return;

            default:
                this.sendPayload("undefined_error", "Unknown signal: " + s);
                return;

        }

    }

    /**
     * Sends an error message to the client
     * 
     * @param message The text explaining the error
     * @param code The code of the error
     * @param uid The uid of the request
     */
    error (
        message: string = "", 
        code: COCKPIT_ERR = COCKPIT_ERR.unknown,
        uid?: string
    ): void {
        
        let msg = `${COCKPIT_ERR_MESSAGE[code]}${message ? ": " + message : ""}`;
        let payload = JSON.stringify({
            code, message: msg
        });

        this.sendPayload("error", payload, uid);

    }

    /**
     * The Client's ID is defined as `<client_type>:<base_id>`
     */
    get id (): string {
        return `${this.type}:${this._id}`;
    }

    set id (_id: string) {
        let old = this.id;
        this._id = _id;
        this.onIDChange({ old, id: this.id });
    }

};
