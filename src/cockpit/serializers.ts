import { CockpitPacket } from "./client";
import { HelpMessage } from "./route";

export type Serializer = {
    id: string;
    f: (packer: CockpitPacket) => string;
};

/**
 * Stringifies the help message for raw sockets
 * @param packet The help packet
 * @returns The stringified message
 */
function serializeHelp (packet: CockpitPacket): string {

    let message = <HelpMessage>JSON.parse(packet.payload);

    let firstLine = `--- ${message.title} - ${message.descritpion} ---`;
    let lastLine = `-`.repeat(firstLine.length);
    return [
        firstLine,
        message.lines
            .map(e => ` - ${e.id} ${e.args.map(a => a.required ? `<${a.name}>` : `[${a.name}]`).join(" ")}: ${e.description}`)
            .join("\r\n"),
        lastLine
    ].filter(e => e != "").join("\r\n");

}

export const DEFAULT_SERIALIZERS: Serializer[] = [
    { id: "help", f: serializeHelp }
];