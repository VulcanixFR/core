import { Logger } from "../logger";
import { SubprocessManager } from "../subprocessManager";
import { SystemVitals } from "../systemVitals";
import { CockpitPacket, CockpitClient } from "./client";
import { parseCommand } from "./commandParser";
import { COCKPIT_ERR } from "./errors";
import { CockpitRoute } from "./route";

/** Request object for Cockpit */
export type CockpitRequest = {

    /**
     * The decomposed command string
     */
    command: string[],

    /**
     * A reference to Cockpit
     */
    cockpit: Cockpit,

    /**
     * A reference to the client making the request
     */
    client: CockpitClient,

    /**
     * The callback to respond to the request
     * @param payload The response to the request
     * @param type The type of response
     */
    respond: (payload: string, type?: string) => void,

    /**
     * The callback to send an error message to the request's client
     * @param payload The error message
     */
    error: (message?: string, code?: COCKPIT_ERR) => void,

};


/**
 * The control center
 * 
 * It extends CockpitRoute in order to be easier to work with
 */
export class Cockpit extends CockpitRoute {

    private clients: CockpitClient[] = [];

    constructor (
        public logger: Logger,
        public vitals: SystemVitals,
        public subprocess: SubprocessManager
    ) {

        super("Cockpit", "Core's socket interface");

    }

    /**
     * Registers a new client to Cockpit
     * @param client The client connecting
     */
    register (client: CockpitClient) {
        
        // Checks if the client is aleready registered
        let known = this.clients.filter(c => c == client)[0];
        if (known) {
            this.logger.error(`Tried to register ${client.id} twice`);
            return;
        }

        // Logging the client's arrival
        this.logger.log(`New client \x1b[1;33m${client.id}\x1b[0m connected`);

        // Adds the client to the internal buffer
        this.clients.push(client);

        // Attaches the client's events to Cockpit
        client.onmessage = msg => {

            // Checks the type of message to decide what to do
            switch (msg.type) {

                case "request":
                    this.request(msg, client);
                    return;

                default:
                    // Echoing the unknown packet type
                    client.sendPayload("echo", msg.payload);

            }

        };

        client.onclose = () => {
            // Removing reference in cockpit's client list
            this.unregister(client);
        }

        client.onIDChange = ({old, id}) => {
            // Logging the client's id change
            this.logger.log(`\x1b[1;33m${old}\x1b[0m was renamed to \x1b[1;33m${id}\x1b[0m`);
        };

    }

    /**
     * Unregisters a client from Cockpit
     * @param client The client disconnected
     */
    unregister (client: CockpitClient) {

        // Removes the client from the internal buffer
        this.clients = this.clients.filter(c => c != client);

        // Unsubscribes the client from system vitals
        this.vitals.unsubscribe(client);

        // Logging the client's departure
        this.logger.log(`\x1b[1;33m${client.id}\x1b[0m has closed the connection`);

    }

    /**
     * Processes a "request" packet
     * @param packet The packet to be processed
     * @param client The client making the request
     */
    request (packet: CockpitPacket, client: CockpitClient) {

        let escapedRequest = packet.payload.replace(/\n/g, "\\n").replace(/\r/g,"\\r");
        this.logger.debug(`Recieved ${packet.uid ? `request ${packet.uid}` : "a request"} by ${packet.client}: ${escapedRequest}`)

        // Callback used to wrap the response without forgetting the unique identifier
        let respond = (payload: string, type?: string) => {
            this.logger.debug(`Responded to ${packet.uid ? `request ${packet.uid}` : `"${escapedRequest}"`} by ${packet.client} with ${type || payload.replace(/\n/g, "\\n").replace(/\r/g,"\\r")}`);
            client.sendPayload(type || 'response', payload, packet.uid);
        };

        // Callback used to wrap the error without forgetting the unique identifier
        let error = (message?: string, error: COCKPIT_ERR = COCKPIT_ERR.unknown) => {
            this.logger.error(`Failed to fullfill ${packet.uid ? `request ${packet.uid}` : `"${escapedRequest}"`} by ${packet.client}`);
            client.error(message, error, packet.uid);
        };

        // Creating the final request to be routed
        let request: CockpitRequest = {
            client: client, 
            cockpit: this,
            command: parseCommand(packet.payload),
            error,
            respond
        };

        // Routing the request through itself
        this.handle(request);

    }

}