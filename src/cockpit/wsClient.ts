import { CockpitClient } from "./client";
import { WebSocket } from "ws";

let id = 0;

/**
 * Cockpit Client implementation for UNIX and TCP sockets
 */
export class CockpitWSClient extends CockpitClient {

    constructor (private socket: WebSocket) {

        super();

        // Initializing the Client's type
        this.type = "ws";
        this.id = (id++).toString();

        // Handling socket close event
        socket.onclose = () => {
            this.onclose();
        };

        // Handling socket data event
        socket.onmessage = msg => {
            this.handleBuffer(<Buffer>msg.data);
        };

        socket.onerror = (err) => {
            // Silent error O_O
        }

    }

    protected sendMessage(data: string): boolean {
        
        // Checks if the socket is closed
        if (this.socket.readyState != 1) return false;

        // When in "normal" mode, nothing is added
        // When in "raw" mode, a chevron is added to make the message
        // look like a shell
        let suffix = this.raw ? "\r\n> " : "";

        // Tries to write to the socket
        try {
            this.socket.send(data + suffix);
        } catch (e) {
            return false;
        }

        return true;

    }

}