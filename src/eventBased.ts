export type EventHandler<T> = (data: T) => void;

export class EventBased {

    /**
     * List of the events listeners
     */
    private _eventListeners: { event: string, handle: EventHandler<any> }[] = [];

    /**
     * Fires an event with the given data
     * @param event Event to fire
     * @param data The data transmitted
     */
    protected _fire <T>(event: string, data: T) {

        this._eventListeners
            .filter(e => e.event == event)
            .forEach(e => {
                try {
                    e.handle(data);
                } catch (e) {
                    console.error("<EventBased>\n", e) ;
                }
            });

    }

    /**
     * Registers an event handler to a peculiar event
     * @param event Event to listen to
     * @param handle Event handler
     */
    public on <T>(event: string, handle: EventHandler<T>) {

        let nb = this._eventListeners.filter(e => e.handle == handle).length;
        if (nb > 0) return; // Not registering more than 1 time the same listener

        this._eventListeners.push({ event, handle });

    }

    /**
     * Removes an event handler
     * @param handle The event handler to remove
     */
    public removeListener <T>(handle: EventHandler<T>) {

        this._eventListeners = this._eventListeners.filter(e => e.handle != handle);

    }

}