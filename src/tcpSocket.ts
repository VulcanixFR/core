import { Server } from "net";
import { Cockpit } from "./cockpit/cockpit";
import { CockpitSocketClient } from "./cockpit/socketClient";

/**
 * Creates a TCP Socket server 
 * @param path Path to the unix socket
 * @returns The server
 */
export function initTCPServer (port: number, cockpit: Cockpit): Server {

    let srv = new Server();
    let logger = cockpit.logger.getInterface("TCP Socket Server");
    
    // Code to log when the socket is opened
    srv.on("listening", () => {
        logger.log(`TCP Socket opened on port \x1b[1;35m${port}\x1b[0m`);
    })

    // Code to handle the new Unix Socket Clients
    srv.on("connection", sock => {

        let client = new CockpitSocketClient(sock, "tcp");
        cockpit.register(client);

    });

    // Opens the socket
    srv.listen(port, "0.0.0.0");
    
    return srv;

}